//? build new obj and add to class
//? making parseFile out of file
// var parseFile = new Parse.File(file.name, file, file.type);
// var className = new Parse.Object("className");
// className.set("state", 1);
// className.set("image", parseFile);
// className.save().then(
//   (success) => {
//     alert("New object created with objectId: " + success.id);
//   },
//   (error) => {
//     alert("Failed to create new object, with error code: " + error.message);
//   }
// );
//? You can delete a single field from an object with the unset method:
// myObject.unset("playerName");
//? update and add object
// myObject.save();
//? To delete an object
// myObject.destroy().then((myObject) => {
// The object was deleted from the Parse Cloud.
// }, (error) => {
// The delete failed.
// error is a Parse.Error with an error code and message.
// });
