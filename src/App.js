import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from "./modules/Dashboard/Dashboard";
import Signinup from "./modules/signin_up/signin_up";
import Web from "./modules/web/router";
import Page404 from "./components/404/index";

function App() {
  window.less
    .modifyVars({
      "@primary-color": "#e89bda",
    })
    .then(() => {
      // console.log("color changed!");
    })
    .catch((error) => {
      console.error(error);
    });

  return (
    <Router>
      <Switch>
        <Route exact path="/Dashboard">
          <Dashboard />
        </Route>
        <Route exact path="/signin_up">
          <Signinup />
        </Route>
        {/* Web */}
        <Route exact path="/">
          <Web />
        </Route>
        <Route exact path="/AboutUs">
          <Web />
        </Route>
        <Route exact path="/ContactUs">
          <Web />
        </Route>
        <Route exact path="/Entertainment">
          <Web />
        </Route>
        <Route exact path="/Articles">
          <Web />
        </Route>
        <Route path="/SingleArticle/:slug">
          <Web />
        </Route>
        <Route path="/singleEntertainment/:slug">
          <Web />
        </Route>
        {/* Web */}
        <Route path="*">
          <Page404 />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
