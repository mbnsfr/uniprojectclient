/* eslint-disable react/react-in-jsx-scope */
import React from "react";
import "../web/master/Master.less";
import {
  Form,
  Input,
  Button,
  Checkbox,
  Row,
  Col,
  Upload,
  Select,
  Alert,
} from "antd";
import { UserOutlined, LockOutlined, UploadOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import signin_upController from "./signin_upController";

const { Option } = Select;

export default () => {
  const {
    onFinish,
    newUser,
    handleSignUp,
    error,
    signIn,
    // Upload
    fileList,
    ontransformFile,
    onRemove,
  } = signin_upController();

  const layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 20,
    },
  };

  const validateMessages = {
    // eslint-disable-next-line
    required: "${label} اجباری است!",
  };

  return (
    <div className="login rtl">
      <Row justify="center" align="middle">
        <Col xl={12} lg={16} md={20} sm={24} xs={24}>
          {signIn && (
            <Form
              name="normal_login"
              className="login-form"
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
            >
              {error && (
                <Form.Item>
                  <Alert
                    message="خطا"
                    description={String(error)}
                    type="error"
                    showIcon
                  />
                </Form.Item>
              )}
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: "لطفا نام کاربری خود را وارد کنید ",
                  },
                ]}
              >
                <Input
                  bordered={false}
                  prefix={<UserOutlined />}
                  placeholder="نام کاربری"
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "لطفا رمزعبور خود را وارد کنید ",
                  },
                ]}
              >
                <Input
                  bordered={false}
                  prefix={<LockOutlined />}
                  type="password"
                  placeholder="رمز عبور"
                />
              </Form.Item>
              <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                  <Checkbox>مرا به خاطر بسپار</Checkbox>
                </Form.Item>
              </Form.Item>
              <Form.Item>
                <Row>
                  <Col span={3}>
                    {/* <Button type="primary">
                      <Link to="/">رمز عبور را فراموش کرده ام</Link>
                    </Button>
                    &nbsp; &nbsp; */}
                    <Button type="primary" onClick={() => handleSignUp()}>
                      ثبت نام
                    </Button>
                  </Col>
                  <Col span={18}></Col>
                  <Col span={3}>
                    <Button type="primary" htmlType="submit">
                      ادامه
                    </Button>
                  </Col>
                </Row>
              </Form.Item>
            </Form>
          )}
          {!signIn && (
            <Form
              {...layout}
              name="nest-messages"
              className="signUp-form"
              initialValues={{
                remember: true,
              }}
              onFinish={newUser}
              validateMessages={validateMessages}
            >
              {error && (
                <Form.Item>
                  <Alert
                    message="خطا"
                    description={String(error)}
                    type="error"
                    showIcon
                  />
                  {/* <div className="error">{String(error)}</div> */}
                </Form.Item>
              )}
              <Form.Item
                label="نام"
                name={["firstName"]}
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="نام خانوادگی"
                name={["lastName"]}
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name={["email"]}
                label="ایمیل"
                rules={[
                  {
                    message: "لطفا ایمیل معتبر وارد کنید",
                    required: true,
                    type: "email",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="نوع کاربری"
                name={["type"]}
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Select>
                  {/* <Option value="user">کاربر</Option> */}
                  <Option value="author">نویسنده</Option>
                </Select>
              </Form.Item>
              <Form.Item
                label="نام کاربری"
                name={["username"]}
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="password"
                label="رمز عبور"
                rules={[
                  {
                    required: true,
                  },
                ]}
                hasFeedback
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="confirm"
                label="تایید رمز عبور"
                dependencies={["password"]}
                hasFeedback
                rules={[
                  {
                    required: true,
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        "دو رمز وارد شده با یکدیگر برابر نیستند"
                      );
                    },
                  }),
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                label="عکس"
                name={["image"]}
                // rules={[
                //   {
                    // required: true,
                //   },
                // ]}
              >
                <Upload
                  accept="image/*"
                  fileList={fileList}
                  transformFile={(file) => ontransformFile(file)}
                  onRemove={(file) => onRemove(file)}
                  disabled={false}
                  maxCount={1}
                >
                  <Button icon={<UploadOutlined />}>&nbsp; &nbsp;آپلود</Button>
                </Upload>
              </Form.Item>
              <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type="primary" htmlType="submit">
                  ثبت نام
                </Button>
                &nbsp; &nbsp;
                <Button type="primary" onClick={() => handleSignUp()}>
                  انصراف
                </Button>
              </Form.Item>
            </Form>
          )}
        </Col>
      </Row>
    </div>
  );
};
