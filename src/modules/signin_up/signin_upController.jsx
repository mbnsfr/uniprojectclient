import { useState } from "react";
import { Parse } from "parse";

export default () => {
  const [error, setError] = useState(null);
  const [signIn, setSignIn] = useState(true);
  // Upload
  const [fileList, setFileList] = useState([]);
  const [file, setFile] = useState(null);

  var currentUser = Parse.User.current();
  if (currentUser) {
    window.location.href = "http://localhost:3000/Dashboard";
  }

  const onFinish = async (values) => {
    // eslint-disable-next-line
    const result = await Parse.User.logIn(
      values.username,
      values.password
    ).then(
      function(user) {
        setError(null);
        window.location.href = "http://localhost:3000/Dashboard";
      },
      function(error) {
        setError(
          "اطلاعات اشتباه وارد شده در صورتی که ثبت نام نکرده اید میتوانید با کلیک بر روی دکمه ثبت نام ثبت نام کنید."
        );
      }
    );
  };

  const newUser = async (values) => {
    var user = new Parse.User();
    user.set("username", values.username);
    user.set("password", values.password);
    user.set("role", values.type);
    user.set("email", values.email);
    user.set("firstName", values.firstName);
    user.set("lastName", values.lastName);
    user.set("image", file);
    user.set("state", 1);

    try {
      await user.signUp();
    } catch (error) {
      console.log(error);
      if (
        error.message === "Account already exists for this username." ||
        error.message === "Account already exists for this email address."
      ) {
        setError(
          "شما قبلا ثبت نام کردید . لطفا از این صفحه انصراف دهید و وارد شوید."
        );
      } else {
        setError(error.message);
      }
    }
  };
  const handleSignUp = () => {
    setError(null);
    setSignIn(!signIn);
  };

  // Upload
  const ontransformFile = (file) => {
    if (file.type !== "") {
      setFile(
        new Parse.File(
          `articles${Math.floor(Math.random() * 100000)}`,
          file,
          file.type
        )
      );
      setFileList([
        {
          uid: "565656",
          name: "عکس کاربر",
          status: "done",
          url: "",
        },
      ]);
    }
  };

  const onRemove = async () => {
    setFileList([]);
  };

  return {
    error,
    signIn,
    onFinish,
    handleSignUp,
    newUser,
    // Upload
    fileList,
    ontransformFile,
    onRemove,
  };
};
