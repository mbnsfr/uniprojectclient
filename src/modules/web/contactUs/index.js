import React from "react";
import {
  Layout,
  Input,
  Button,
  Row,
  Col,
  Typography,
  Divider,
  Spin,
  Form,
  Carousel,
  Image,
  BackTop,
} from "antd";
import {
  EnvironmentOutlined,
  LinkedinOutlined,
  YoutubeOutlined,
  PhoneOutlined,
  MailOutlined,
  FacebookOutlined,
  InstagramOutlined,
  TwitterOutlined,
  UpOutlined,
  WhatsAppOutlined,
} from "@ant-design/icons";
import carousel from "../../../assets/images/carousel2.jpg";
import controller from "./controller";

const { TextArea } = Input;
const { Content } = Layout;
const { Paragraph } = Typography;

export default () => {
  const { onFinish } = controller();

  const validateMessages = {
    // eslint-disable-next-line
    required: "${label} اجباری است!",
  };

  const layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 20,
    },
  };

  return (
    <Spin spinning={false}>
      <Content>
        <Carousel autoplay>
          <Image
            className="img-responsive"
            alt="carousel"
            src={carousel}
          ></Image>
        </Carousel>
      </Content>
      <BackTop>
        <Button size="large" shape="circle">
          <UpOutlined />
        </Button>
      </BackTop>
      <Content className="inner-content rtl">
        <Row gutter={[24, 8]}>
          <Col xl={16} lg={16} md={16} sm={24} xs={24}>
            <Form
              {...layout}
              onFinish={onFinish}
              validateMessages={validateMessages}
            >
              <Form.Item
                label="نام"
                name="name"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input placeholder="نام*" />
              </Form.Item>
              <Form.Item
                label="ایمیل"
                name="email"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input placeholder="ایمیل*" />
              </Form.Item>
              <Form.Item
                label="متن پیام"
                name="text"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <TextArea placeholder="پیام*" rows={4} />
              </Form.Item>
              <Button htmlType="submit" size="large" type="primary">
                ارسال
              </Button>
            </Form>
          </Col>
          <Col xl={8} lg={8} md={8} sm={24} xs={24}>
            <div>راه های ارتباطی ما</div>
            <Paragraph>
              <InstagramOutlined />
              &nbsp; &nbsp;
              <FacebookOutlined />
              &nbsp; &nbsp;
              <TwitterOutlined />
              &nbsp; &nbsp;
              <LinkedinOutlined />
              &nbsp; &nbsp;
              <YoutubeOutlined />
              &nbsp; &nbsp;
            </Paragraph>
            <Divider />
            <div>شماره</div>
            <Paragraph>
              <PhoneOutlined />
              &nbsp; &nbsp;
              <p
                style={{ color: "black", textAlign: "right", direction: "ltr" }}
              >
                021 - 22 442831
              </p>
              <br />
              <WhatsAppOutlined />
              &nbsp; &nbsp;
              <p
                style={{ color: "black", textAlign: "right", direction: "ltr" }}
              >
                +98 930 3612630
              </p>
            </Paragraph>
            <Divider />
            <div>ایمیل</div>
            <Paragraph>
              <MailOutlined />
              &nbsp; mbnsfr@gmail.com
            </Paragraph>
            <Divider />
            <div> نشانی</div>
            <Paragraph>
              <EnvironmentOutlined />
              &nbsp; تهران - میدان ازگل
            </Paragraph>
            <Divider />
          </Col>
        </Row>
      </Content>
    </Spin>
  );
};
