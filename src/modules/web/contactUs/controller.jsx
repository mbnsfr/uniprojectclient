import { Parse } from "parse";

export default () => {
  const onFinish = (values) => {
    const Requests = Parse.Object.extend("Requests");
    const requests = new Requests();

    requests.set("name", values.name);
    requests.set("email", values.email);
    requests.set("text", values.text);

    requests.save().then(
      (requests) => {
        alert("پیام شما با موفقیت ارسال گردید.");
        window.location.reload();
      },
      (error) => {
        window.location.reload();
      }
    );
  };

  return {
    onFinish,
  };
};
