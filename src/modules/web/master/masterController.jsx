import React, { useState, useEffect } from "react";
// import { Parse } from "parse";

export default () => {
  const [drawer, setDrawer] = useState(false);

  //   useEffect(() => {
  //     const getSetting = async () => {
  //       const Setting = Parse.Object.extend('Setting');
  //       const query = new Parse.Query(Setting);
  //       query.equalTo('state', 2);
  //       const result = await query.first();
  //       if (result) {
  //         setSetting(result);
  //         setHasData(!hasData);
  //       }
  //     };
  //   })

  return {
    drawer,
    setDrawer,
  };
};
