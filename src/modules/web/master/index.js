import React from "react";
import "./Master.less";
import { Button, Layout, Row, Col, Divider, Spin } from "antd";
import {
  InstagramOutlined,
  YoutubeOutlined,
  TwitterOutlined,
  GooglePlusOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import logo from "../../../assets/images/logo.png";
import logoB from "../../../assets/images/logo.png";
const { Header, Footer, Content } = Layout;

export default (props) => {
  // window.scrollTo(0, 0);
  return (
    <Spin spinning={false}>
      <Layout>
        <Header class="header">
          <Row>
            <Col span={1}></Col>
            <Col span={5}>
              <Link to="/">
                <img alt="img" src={logo} style={{ width: "100px" }} />
              </Link>
            </Col>
            <Col span={15}></Col>
            <Col span={3}>
              <Link to="/signin_up" target="_blank">
                <Button style={{ marginTop: "34px" }}>ورود / ثبت‌نام</Button>
              </Link>
            </Col>
          </Row>
        </Header>
        <Content>
          <Row>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              {props.children}
            </Col>
          </Row>
        </Content>
        <Footer className="footer">
          <Row>
            <Col xl={5} lg={5} md={5} sm={24} xs={24}>
              <img alt="img" className="img-responsive" src={logoB} />
            </Col>
            <Col xl={4} lg={4} md={4} sm={0} xs={0}></Col>
            <Col xl={3} lg={3} md={3} sm={24} xs={24}>
              <Row gutter={[0, 24]}>
                <Col span={24}>
                  <Link to="/AboutUs" target="_blank">
                    ارتباط با‌ما
                  </Link>
                </Col>
                <Col span={24}>
                  <Link to="/ContactUs" target="_blank">
                    تماس با‌ما
                  </Link>
                </Col>
              </Row>
            </Col>
            <Col xl={1} lg={1} md={1} sm={0} xs={0}></Col>
            <Col xl={8} lg={8} md={8} sm={24} xs={24}>
              {/* eslint-disable-next-line jsx-a11y/iframe-has-title */}
              <iframe
                style={{ borderRadius: "10px", overflowX: "hidden" }}
                width="100%"
                height="100%"
                id="mapcanvas"
                src="https://www.google.com/maps?q=35.741417,51.410424&amp;t&amp;z=18&amp;ie=UTF8&amp;iwloc&amp;output=embed"
                frameborder="0"
                scrolling="no"
                marginheight="0"
                marginwidth="0"
              >
                <div class="zxos8_gm">
                  <a href="https://www.vouchersort.co.uk/how-to-bypass-the-queue-it-system-2020">
                    Tesco slots
                  </a>
                </div>
                <div style={{ overflow: "hidden" }}>
                  <div
                    id="gmap_canvas"
                    style={{ height: "100%", width: "100%" }}
                  ></div>
                </div>
                <div>
                  <small>
                    Powered by
                    <a href="https://www.embedgooglemap.co.uk">
                      Embed Google Map
                    </a>
                  </small>
                </div>
              </iframe>
            </Col>
          </Row>
          <Divider />
          <div
            style={{ width: "100%", display: "flex", justifyContent: "center" }}
          >
            <InstagramOutlined style={{ fontSize: "20px", margin: "4px" }} />
            <GooglePlusOutlined style={{ fontSize: "20px", margin: "4px" }} />
            <YoutubeOutlined style={{ fontSize: "20px", margin: "4px" }} />
            <TwitterOutlined style={{ fontSize: "20px", margin: "4px" }} />
          </div>
        </Footer>
      </Layout>
    </Spin>
  );
};
