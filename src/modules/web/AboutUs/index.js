import React from "react";
import { Layout, Row, Col, Spin, Image, Carousel, BackTop ,Button} from "antd";
import { UpOutlined } from "@ant-design/icons";
import carousel from "../../../assets/images/carousel1.jpg";

const { Content } = Layout;

export default () => {
  return (
    <Spin spinning={false}>
      <Content>
        <Carousel autoplay>
          <Image
            className="img-responsive"
            alt="carousel"
            src={carousel}
          ></Image>
        </Carousel>
      </Content>
      <BackTop>
        <Button size="large" shape="circle">
          <UpOutlined />
        </Button>
      </BackTop>
      <Content className="inner-content">
        <Row gutter={[24, 24]}>
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            <h1>درباره سایت اموزش و سرگرمی کودکان </h1>
            <p>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
              استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
              در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
              نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد،
              کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان
              جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای
              طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان
              فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
              موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد
              نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل
              دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </p>
          </Col>
        </Row>
      </Content>
    </Spin>
  );
};
