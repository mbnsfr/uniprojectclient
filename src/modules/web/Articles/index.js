import React from "react";
import {
  Typography,
  Divider,
  Layout,
  Row,
  Col,
  Spin,
  BackTop,
  Button,
  Select,
  Card,
  Avatar,
  Alert,
  Pagination,
  List,
} from "antd";
import { UpOutlined } from "@ant-design/icons";
import controller from "./controller";
import avatar from "../../../assets/images/profile.jpg";

const { Title } = Typography;
const { Content } = Layout;
const { Option } = Select;
const { Meta } = Card;

export default () => {
  const { loading, article, selectHandler } = controller();

  const obj = {
    "education-artistic": "آموزشی-هنری",
    "education-Cultural": "آموزشی-فرهنگی",
    "education-Science": "آموزشی-علمی و مهارتی",
    "education-Health": "آموزشی-بهداشتی",
    "news-award": "اخبار-جشنواره ها",
  };

  return (
    <Spin spinning={loading}>
      <Content className="inner-content rtl">
        <div class="spectrum-background">
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <Title class="center" style={{ fontSize: "45px" }}>
            مقالات
          </Title>
        </div>
        <BackTop>
          <Button size="large" shape="circle">
            <UpOutlined />
          </Button>
        </BackTop>
        <Row gutter={[48, 48]}>
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            {article && (
              <List
                header={
                  <Select
                    defaultValue="all"
                    style={{ minWidth: "200px" }}
                    onChange={(e) => selectHandler(e)}
                  >
                    <Option value="all">همه</Option>
                    {Object.entries(obj).map((item) => {
                      return <Option value={String(item[0])}>{item[1]}</Option>;
                    })}
                  </Select>
                }
                grid={{
                  gutter: 48,
                  xs: 1,
                  sm: 1,
                  md: 2,
                  lg: 3,
                  xl: 3,
                  xxl: 3,
                }}
                pagination={{
                  pageSize: 6,
                }}
                dataSource={article}
                renderItem={(item) => (
                  <List.Item>
                    <a href={"/SingleArticle/" + item.id}>
                      <Card
                        className="home-card"
                        hoverable
                        cover={<img alt="example" src={item.image} />}
                      >
                        <p
                          style={{
                            borderRadius: "10px",
                            width: "200px",
                            backgroundColor: "#fbe2f2",
                            textAlign: "center",
                            padding: "3px",
                            marginBottom: "10px",
                          }}
                        >
                          {
                            Object.values(obj)[
                              Object.keys(obj).indexOf(item.type)
                            ]
                          }
                        </p>
                        <Meta
                          avatar={<Avatar src={avatar} />}
                          title={"عنوان : " + item.title}
                          description={"نویسنده : " + item.author}
                        />
                      </Card>
                    </a>
                  </List.Item>
                )}
              />
            )}
          </Col>
        </Row>
      </Content>
    </Spin>
  );
};
