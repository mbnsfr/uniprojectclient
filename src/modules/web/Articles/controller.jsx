import { useState, useEffect } from "react";
import { Parse } from "parse";

export default () => {
  const [loading, setLoading] = useState(true);
  const [article, setArticle] = useState(null);
  const [category, setCategory] = useState("all");

  useEffect(() => {
    const getArticles = async () => {
      const className = Parse.Object.extend("Articles");
      const query = new Parse.Query(className);
      if (category !== "all") {
        query.equalTo("type", String(category));
      }
      query.equalTo("state", 2);
      const result = await query.find();
      if (result) {
        let arr = [];
        result.map((item) => {
          arr.push({
            id: item.id,
            image: item.get("image").url(),
            type: item.get("type"),
            title: item.get("title"),
            author: item.get("author"),
          });
        });
        setArticle(arr);
      }
    };

    getArticles();
    setLoading(false);
  }, [category]);

  const selectHandler = (e) => {
    setCategory(e);
  };

  return {
    loading,
    article,
    selectHandler,
  };
};
