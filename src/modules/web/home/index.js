import React from "react";
import {
  Row,
  Col,
  Carousel,
  Spin,
  Image,
  BackTop,
  Button,
  Layout,
  Card,
  Tabs,
  Avatar,
} from "antd";
import carousel5 from "../../../assets/images/carousel5.jpg";
import carousel1 from "../../../assets/images/carousel1.jpg";
import carousel2 from "../../../assets/images/carousel2.jpg";
import image from "../../../assets/images/online-game.jpeg";
import book from "../../../assets/images/book.jpg";
import image1 from "../../../assets/images/signin_up.jpg";
import "../master/Master.less";
import { UpOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import avatar from "../../../assets/images/profile.jpg";

const { Content } = Layout;
const { TabPane } = Tabs;
const { Meta } = Card;

export default () => {
  return (
    <Spin spinning={false}>
      <Content className="unset">
        <Carousel autoplay>
          <Image
            className="img-responsive"
            alt="carousel"
            src={carousel5}
            height="100"
          ></Image>
          <div>
            <Image
              className="img-responsive"
              alt="carousel"
              src={carousel1}
              height="100"
            ></Image>
          </div>
          <div>
            <Image
              className="img-responsive"
              alt="carousel"
              src={carousel2}
              height="100"
            ></Image>
          </div>
        </Carousel>
      </Content>
      <BackTop>
        <Button size="large" shape="circle">
          <UpOutlined />
        </Button>
      </BackTop>
      <Content className="inner-content rtl">
        <Row gutter={[48, 48]}>
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            <Row>
              <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                <h1>آموزشی</h1>
              </Col>
            </Row>
            <Tabs
              defaultActiveKey="1"
              onChange={(e) => {
                console.log(e);
              }}
              activeTabBarStyle={{
                backgroundColor: "red",
              }}
              tabBarExtraContent={<Button>مشاهده بیشتر</Button>}
            >
              <TabPane tab="هنری" key="1">
                <Row>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Card
                      className="home-card"
                      hoverable
                      cover={<img alt="example" src={image1} />}
                    >
                      <Meta
                        title="هنری ۱"
                        description=" اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                      />
                    </Card>
                  </Col>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Card
                      className="home-card"
                      hoverable
                      cover={<img alt="example" src={image1} />}
                    >
                      <Meta
                        title="هنری ۲"
                        description=" اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                      />
                    </Card>
                  </Col>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Card
                      className="home-card"
                      hoverable
                      cover={<img alt="example" src={image1} />}
                    >
                      <Meta
                        title="هنری ۳"
                        description=" اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                      />
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="فرهنگی" key="2">
                فرهنگی
              </TabPane>
              <TabPane tab="علمی و مهارتی" key="3">
                علمی و مهارتی
              </TabPane>
              <TabPane tab="بهداشتی" key="4">
                بهداشتی
              </TabPane>
            </Tabs>
            <br />
            <br />
            <Row>
              <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                <h1>سرگرمی</h1>
              </Col>
            </Row>
            <Tabs
              defaultActiveKey="1"
              onChange={(e) => {
                console.log(e);
              }}
              activeTabBarStyle={{
                backgroundColor: "red",
              }}
              tabBarExtraContent={<Button>مشاهده بیشتر</Button>}
            >
              <TabPane tab="بازی آنلاین" key="1">
                <Row>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Card
                      className="entertainment-card"
                      hoverable
                      cover={<img alt="example" src={image} />}
                    >
                      <Meta title="بازی آنلاین ۱" />
                    </Card>
                  </Col>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Card
                      className="entertainment-card"
                      hoverable
                      cover={<img alt="example" src={image} />}
                    >
                      <Meta title="بازی آنلاین ۲" />
                    </Card>
                  </Col>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Card
                      className="entertainment-card"
                      hoverable
                      cover={<img alt="example" src={image} />}
                    >
                      <Meta title="بازی آنلاین ۳" />
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="جدول" key="2">
                جدول
              </TabPane>
              <TabPane tab="چیستان" key="3">
                چیستان
              </TabPane>
              <TabPane tab="جوک" key="4">
                جوک
              </TabPane>
              <TabPane tab="کويیز" key="4">
                کويیز
              </TabPane>
            </Tabs>
            <br />
            <br />
            <Row>
              <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                <h1>اخبار</h1>
              </Col>
            </Row>
            <Tabs
              defaultActiveKey="1"
              onChange={(e) => {
                console.log(e);
              }}
              activeTabBarStyle={{
                backgroundColor: "red",
              }}
              tabBarExtraContent={<Button>مشاهده بیشتر</Button>}
            >
              <TabPane tab="جشنواره ها" key="1">
                <Row>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Link to="/SingleArticle">
                      <Card className="home-card">
                        <Meta
                          avatar={<Avatar src={avatar} />}
                          title="جشنواره ها ۱"
                          description="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                        />
                      </Card>
                    </Link>
                  </Col>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Link to="/SingleArticle">
                      <Card className="home-card">
                        <Meta
                          avatar={<Avatar src={avatar} />}
                          title="جشنواره ها ۲"
                          description="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                        />
                      </Card>
                    </Link>
                  </Col>
                  <Col xl={8} lg={8} md={12} sm={24} xs={24}>
                    <Link to="/SingleArticle">
                      <Card className="home-card">
                        <Meta
                          avatar={<Avatar src={avatar} />}
                          title="جشنواره ها ۳"
                          description="لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                        />
                      </Card>
                    </Link>
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
            <br />
            <br />
            <Row>
              <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                <h1>کتابخانه</h1>
              </Col>
            </Row>
            <Tabs
              defaultActiveKey="1"
              onChange={(e) => {
                console.log(e);
              }}
              activeTabBarStyle={{
                backgroundColor: "red",
              }}
              tabBarExtraContent={<Button>مشاهده بیشتر</Button>}
            >
              <TabPane tab="داستان صوتی" key="1">
                <Row>
                  <Col xl={6} lg={6} md={8} sm={12} xs={12}>
                    <Card
                      className="entertainment-card"
                      hoverable
                      cover={<img alt="example" src={book} />}
                    >
                      <Meta
                        title="داستان صوتی ۱"
                        description=" اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                      />
                    </Card>
                  </Col>
                  <Col xl={6} lg={6} md={8} sm={12} xs={12}>
                    <Card
                      className="entertainment-card"
                      hoverable
                      cover={<img alt="example" src={book} />}
                    >
                      <Meta
                        title="داستان صوتی ۲"
                        description=" اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                      />
                    </Card>
                  </Col>
                  <Col xl={6} lg={6} md={8} sm={12} xs={12}>
                    <Card
                      className="entertainment-card"
                      hoverable
                      cover={<img alt="example" src={book} />}
                    >
                      <Meta
                        title="داستان صوتی ۳"
                        description=" اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد."
                      />
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="کتاب" key="2">
                کتاب
              </TabPane>
            </Tabs>
          </Col>
        </Row>
        {/* <Row gutter={[48, 48]}>
          <Col xl={3} lg={3} md={2} sm={0} xs={0}></Col>
          <Col xl={9} lg={9} md={10} sm={24} xs={24}>
        */}
      </Content>
    </Spin>
  );
};
