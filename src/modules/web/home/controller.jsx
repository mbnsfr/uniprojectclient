import { Parse } from "parse";

export default () => {
  // useEffect(() => {
  //   const getEntertainment = async () => {
  //     const className = Parse.Object.extend("Book");
  //     const query = new Parse.Query(className);
  //     const result = await query.find();
  //     if (result) {
  //       setEntertainment(result);
  //     }
  //   };

  //   getEntertainment();
  //   setLoading(false);
  // }, [edit, refresh]);

  const onFinish = (values) => {
    const Requests = Parse.Object.extend("Requests");
    const requests = new Requests();

    requests.set("name", values.name);
    requests.set("email", values.email);
    requests.set("text", values.text);

    requests.save().then(
      (requests) => {
        alert("پیام شما با موفقیت ارسال گردید.");
        window.location.reload();
      },
      (error) => {
        window.location.reload();
      }
    );
  };

  return {
    onFinish,
  };
};
