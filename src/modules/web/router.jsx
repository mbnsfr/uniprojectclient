import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./home/index";
import Master from "./master/index";
import ContactUs from "./contactUs/index";
import AboutUs from "./AboutUs/index";
import SingleArticle from "./singleArticle/index";
import SingleEntertainment from "./singleEntertainment/index";
import Entertainment from "./Entertainment/index";
import Articles from "./Articles/index";

export default () => {
  return (
    <Router>
      <Master>
        <Route exact path="/">
          <Home />
        </Route>
        <Switch>
          <Route path="/AboutUs">
            <AboutUs />
          </Route>
          <Route path="/SingleArticle">
            <SingleArticle />
          </Route>
          <Route path="/SingleEntertainment">
            <SingleEntertainment />
          </Route>
          <Route path="/ContactUs">
            <ContactUs />
          </Route>
          <Route path="/Entertainment">
            <Entertainment />
          </Route>
          <Route path="/Articles">
            <Articles />
          </Route>
        </Switch>
      </Master>
    </Router>
  );
};
