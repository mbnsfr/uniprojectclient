import { useState, useEffect } from "react";
import { Parse } from "parse";

export default () => {
  const [loading, setLoading] = useState(true);
  const [edit, setEdit] = useState(false);
  const [entertainment, setEntertainment] = useState(null);
  // Upload
  const [fileList, setFileList] = useState([]);
  const [file, setFile] = useState(null);
  const [fileList2, setFileList2] = useState([]);
  const [file2, setFile2] = useState(null);
  // article view
  const [view, setView] = useState(false);
  const [selected, setSelected] = useState(null);
  // refresh
  const [refresh, setRefresh] = useState(false);

  const onEdit = () => {
    setFileList([]);
    setFile(null);
    setFileList2([]);
    setFile2(null);
    setEdit(!edit);
  };

  useEffect(() => {
    const getEntertainment = async () => {
      const className = Parse.Object.extend("Entertainment");
      const query = new Parse.Query(className);
      query.equalTo("type", "download");
      const result = await query.find();
      if (result) {
        setEntertainment(result);
      }
    };

    getEntertainment();
    setLoading(false);
  }, [edit, refresh]);

  // articleForm
  const onFinish = (values) => {
    const Entertainment = Parse.Object.extend("Entertainment");
    const entertainment = new Entertainment();

    entertainment.set("name", values.user.name);
    entertainment.set("text", values.user.text);
    entertainment.set("image", file);
    entertainment.set("file", file2);
    entertainment.set("state", 1);
    entertainment.set("type", "download");

    entertainment.save().then(
      (entertainment) => {
        setEdit(!edit);
        // alert("New object created with objectId: " + entertainment.id);
      },
      (error) => {
        setEdit(!edit);
        // alert("Failed to create new object, with error code: " + error.message);
      }
    );
  };

  // Upload
  const ontransformFile = (file) => {
    if (file.type !== "") {
      setFile(
        new Parse.File(
          `entertainment${Math.floor(Math.random() * 100000)}`,
          file,
          file.type
        )
      );
      setFileList([
        {
          uid: "565656",
          name: "عکس کاور سرگرمی",
          status: "done",
          url: "",
        },
      ]);
    }
  };

  const onRemove = async () => {
    setFileList([]);
  };

  const ontransformFile2 = (file) => {
    if (file.type !== "") {
      setFile2(
        new Parse.File(
          `entertainment${Math.floor(Math.random() * 100000)}`,
          file,
          file.type
        )
      );
      setFileList2([
        {
          uid: "565656",
          name: "فایل سرگرمی",
          status: "done",
          url: "",
        },
      ]);
    }
  };

  const onRemove2 = async () => {
    setFileList2([]);
  };

  // article view
  const onView = (e, item) => {
    setView(!view);
    setSelected(item);
  };

  const onBack = () => {
    setView(!view);
  };

  // article delete
  const onDelete = (e, item) => {
    const className = Parse.Object.extend("Entertainment");
    const query = new Parse.Query(className);
    query.get(item.id).then(
      (success) => {
        success.destroy({});
        setRefresh(!refresh);
      },
      (error) => {
        setRefresh(!refresh);
        // console.log("error", error);
      }
    );
  };

  // article confirm
  const onConfirm = (e, item) => {
    const Entertainment = Parse.Object.extend("Entertainment");
    const entertainment = new Entertainment();
    entertainment.set("objectId", item.id);
    entertainment.save().then((result) => {
      result.set("state", 2);
      return result.save();
    });
    setRefresh(!refresh);
  };

  return {
    loading,
    edit,
    entertainment,
    onEdit,
    // articleForm
    onFinish,
    // Upload
    fileList,
    ontransformFile,
    onRemove,
    fileList2,
    ontransformFile2,
    onRemove2,
    // article view
    view,
    selected,
    onView,
    onBack,
    // article delete
    onDelete,
    // article confirm
    onConfirm,
  };
};
// // article Edit
// const [editing, setEditing] = useState(false);
// const [selectedList, setSelectedList] = useState(null);
// // return
// onEditing,
// selectedList,
// editing,
// onEditingBack,
// // articleForm
// onFinishEdit,
// // article Edit
// const onEditing = (e, item) => {
//   setFileList([
//     {
//       uid: item.id,
//       name: item.get("image").name(),
//       status: "done",
//       url: item.get("image").url(),
//     },
//   ]);
//   setFileList2([
//     {
//       uid: item.id,
//       name: item.get("file").name(),
//       status: "done",
//       url: item.get("file").url(),
//     },
//   ]);
//   setSelectedList({
//     name: item.get("name"),
//     text: item.get("text"),
//   });
//   setEditing(!editing);
// };

// const onEditingBack = (e, item) => {
//   setSelectedList(null);
//   setFileList([]);
//   setFile(null);
//   setFileList2([]);
//   setFile2(null);
//   setEditing(!editing);
// };
// // articleFormEdit
// const onFinishEdit = (values) => {
//   const Entertainment = Parse.Object.extend("Entertainment");
//   const entertainment = new Entertainment();

//   entertainment.set("name", values.user.name);
//   entertainment.set("text", values.user.text);
//   entertainment.set("image", file);
//   entertainment.set("file", file2);
//   entertainment.set("state", 1);
//   entertainment.set("type", "download");

//   entertainment.save().then(
//     (entertainment) => {
//       setEditing(!editing);
//       alert("New object created with objectId: " + entertainment.id);
//     },
//     (error) => {
//       alert("Failed to create new object, with error code: " + error.message);
//     }
//   );
// };
