import React from "react";
import {
  Layout,
  Row,
  Col,
  Spin,
  Tabs,
  Card,
  Button,
  Form,
  Input,
  Upload,
  Typography,
  Empty,
} from "antd";
import {
  CheckCircleFilled,
  UploadOutlined,
  PlusCircleFilled,
  CloseCircleFilled,
} from "@ant-design/icons";
import entertaimentController from "./downloadEntetainmentController";
import download from "../../../assets/images/tenor.gif";

const { Content } = Layout;
const { TabPane } = Tabs;

// articleForm
const { TextArea } = Input;

// article view
const { Title, Link } = Typography;

// Form
const layout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 20,
  },
};

const validateMessages = {
  // eslint-disable-next-line
  required: "${label} اجباری است!",
};

export default () => {
  const {
    loading,
    edit,
    entertainment,
    onEdit,
    // articleForm
    onFinish,
    // Upload
    fileList,
    ontransformFile,
    onRemove,
    fileList2,
    ontransformFile2,
    onRemove2,
    // article view
    selected,
    view,
    onView,
    onBack,
    // article delete
    onDelete,
    // article confirm
    onConfirm,
  } = entertaimentController();

  return (
    <Spin spinning={loading}>
      <Content className="inner-content rtl">
        {!edit && !view && (
          <Tabs
            defaultActiveKey="1"
            tabBarExtraContent={
              <Button icon={<PlusCircleFilled />} onClick={() => onEdit()}>
                &nbsp; &nbsp; ایجاد سرگرمی جدید
              </Button>
            }
          >
            <TabPane tab="تایید شده" key="1">
              <Row gutter={[24, 24]}>
                {entertainment &&
                  entertainment.map((item) => {
                    if (item.get("state") === 2) {
                      return (
                        <Col xl={8} lg={12} md={12} sm={24} xs={24}>
                          <Card
                            className="dashboard-card"
                            title="کارت سرگرمی تایید شده"
                            extra={
                              <Button
                                icon={<CheckCircleFilled />}
                                onClick={(e) => onView(e, item)}
                              >
                                &nbsp; &nbsp; مشاهده
                              </Button>
                            }
                          >
                            <p>اسم : {item.get("name")}</p>
                          </Card>
                        </Col>
                      );
                    }
                  })}
                {entertainment == null && (
                  <>
                    <Col xl={11} lg={11} md={11} sm={11} xs={11}></Col>
                    <Col xl={4} lg={4} md={4} sm={4} xs={4}>
                      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    </Col>
                  </>
                )}
              </Row>
            </TabPane>
            <TabPane tab="در انتظار تایید" key="2">
              <Row gutter={[24, 24]}>
                {entertainment &&
                  entertainment.map((item) => {
                    if (item.get("state") === 1) {
                      return (
                        <Col xl={8} lg={12} md={12} sm={24} xs={24}>
                          <Card
                            className="dashboard-card"
                            title="کارت سرگرمی در انتظار تایید"
                            extra={
                              <>
                                <Button
                                  style={{ marginTop: "3px" }}
                                  icon={<CloseCircleFilled />}
                                  onClick={(e) => onDelete(e, item)}
                                >
                                  &nbsp; &nbsp; حذف
                                </Button>
                                <br />
                                <Button
                                  style={{ marginTop: "3px" }}
                                  icon={<CheckCircleFilled />}
                                  onClick={(e) => onConfirm(e, item)}
                                >
                                  &nbsp; &nbsp; تایید
                                </Button>
                              </>
                            }
                          >
                            <p>اسم : {item.get("name")}</p>
                          </Card>
                        </Col>
                      );
                    }
                  })}
                {entertainment == null && (
                  <>
                    <Col xl={11} lg={11} md={11} sm={11} xs={11}></Col>
                    <Col xl={4} lg={4} md={4} sm={4} xs={4}>
                      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    </Col>
                  </>
                )}
              </Row>
            </TabPane>
          </Tabs>
        )}
        {edit && (
          <Form
            {...layout}
            name="nest-messages"
            onFinish={onFinish}
            validateMessages={validateMessages}
          >
            <Form.Item
              label="اسم"
              name={["user", "name"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="توضیحات"
              name={["user", "text"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <TextArea showCount maxLength={500} />
            </Form.Item>
            <Form.Item
              label="عکس"
              name={["user", "image"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Upload
                accept="image/*"
                fileList={fileList}
                transformFile={(file) => ontransformFile(file)}
                onRemove={(file) => onRemove(file)}
                disabled={false}
                maxCount={1}
              >
                <Button icon={<UploadOutlined />}>&nbsp; &nbsp;آپلود</Button>
              </Upload>
            </Form.Item>
            <Form.Item
              label="فایل سرگرمی"
              name={["user", "file"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Upload
                fileList={fileList2}
                transformFile={(file) => ontransformFile2(file)}
                onRemove={(file) => onRemove2(file)}
                disabled={false}
                maxCount={1}
              >
                <Button icon={<UploadOutlined />}>&nbsp; &nbsp;آپلود</Button>
              </Upload>
            </Form.Item>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
              <Button type="primary" htmlType="submit">
                ثبت سرگرمی
              </Button>
              &nbsp; &nbsp;
              <Button type="primary" htmlType="button" onClick={onEdit}>
                انصراف
              </Button>
            </Form.Item>
          </Form>
        )}
        {view && (
          <Row gutter={[48, 48]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Button onClick={() => onBack()}>بازگشت</Button>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Title
                style={{
                  textAlign: "center",
                }}
                level={2}
              >
                {selected.get("name")}
              </Title>
            </Col>
            <Col xl={6} lg={4} md={3} sm={3} xs={3}></Col>
            <Col xl={12} lg={16} md={18} sm={18} xs={18}>
              <img
                alt="entertainment"
                className="img-responsive"
                style={{ borderRadius: "30px " }}
                src={selected.get("image").url()}
              />
            </Col>
            <Col xl={6} lg={4} md={3} sm={3} xs={3}></Col>
            <Col xl={1} lg={1} md={1} sm={1} xs={1}></Col>
            <Col xl={22} lg={22} md={22} sm={22} xs={22}>
              <p>{selected.get("text")}</p>
            </Col>
            <Col xl={1} lg={1} md={1} sm={1} xs={1}></Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Title
                style={{
                  textAlign: "center",
                }}
                level={3}
              >
                دانلود فایل سرگرمی
              </Title>
            </Col>
            <Col xl={10} lg={9} md={8} sm={7} xs={6}></Col>
            <Col xl={4} lg={6} md={8} sm={10} xs={12}>
              <Link href={selected.get("file").url()} target="_blank">
                <img
                  alt="entertainment"
                  className="img-responsive"
                  style={{ borderRadius: "30px " }}
                  src={download}
                />
              </Link>
            </Col>
            <Col xl={10} lg={9} md={8} sm={7} xs={6}></Col>
          </Row>
        )}
      </Content>
    </Spin>
  );
};

// form editing
{
  /* <Button
  icon={<QuestionCircleFilled />}
  onClick={(e) => onEditing(e, item)}
>
  &nbsp; &nbsp; ویرایش
</Button>
<br /> */
}

{
  /* {editing && (
  <Form
    {...layout}
    name="nest-messages"
    onFinish={onFinishEdit}
    validateMessages={validateMessages}
  >
    <Form.Item label="اسم" name={["user", "name"]}>
      <Input defaultValue={selectedList.name} />
    </Form.Item>
    <Form.Item label="توضیحات" name={["user", "text"]}>
      <TextArea
        showCount
        maxLength={200}
        defaultValue={selectedList.text}
      />
    </Form.Item>
    <Form.Item label="عکس" name={["user", "image"]}>
      <Upload
        accept="image/*"
        fileList={fileList}
        transformFile={(file) => ontransformFile(file)}
        onRemove={(file) => onRemove(file)}
        disabled={false}
        maxCount={1}
      >
        <Button icon={<UploadOutlined />}>&nbsp; &nbsp;آپلود</Button>
      </Upload>
    </Form.Item>
    <Form.Item label="فایل سرگرمی" name={["user", "file"]}>
      <Upload
        fileList={fileList2}
        transformFile={(file) => ontransformFile2(file)}
        onRemove={(file) => onRemove2(file)}
        disabled={false}
        maxCount={1}
      >
        <Button icon={<UploadOutlined />}>&nbsp; &nbsp;آپلود</Button>
      </Upload>
    </Form.Item>
    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
      <Button type="primary" htmlType="submit">
        ثبت تغییرات
      </Button>
      &nbsp; &nbsp;
      <Button type="primary" htmlType="button" onClick={onEditingBack}>
        بازگشت
      </Button>
    </Form.Item>
  </Form>
)} */
}
