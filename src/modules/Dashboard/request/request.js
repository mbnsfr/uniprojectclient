import React from "react";
import { Layout, Spin, Table } from "antd";
import requestController from "./requestController";

const { Content } = Layout;

const columns = [
  { title: "نام", dataIndex: "name", key: "name" },
  { title: "ایمیل", dataIndex: "email", key: "email" },
  { title: "تاریخ ایجاد درخواست", dataIndex: "date", key: "date" },
];

export default () => {
  const { data, loading } = requestController();

  return (
    <Spin spinning={loading}>
      <Content className="inner-content">
        <Table
          className="components-table-demo-nested"
          columns={columns}
          expandable={{ expandedRowRender: (record) => <p>{record.text}</p> }}
          dataSource={data}
        />
      </Content>
    </Spin>
  );
};
