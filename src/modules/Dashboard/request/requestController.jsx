import { useState, useEffect } from "react";
import { Parse } from "parse";

export default () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    const getRequests = async () => {
      const className = Parse.Object.extend("Requests");
      const query = new Parse.Query(className);
      const result = await query.find();
      if (result) {
        let arr = [];
        result.map((item, key) => {
          arr.push({
            key: key,
            date: String(item.createdAt),
            name: item.get("name"),
            email: item.get("email"),
            text: item.get("text"),
          });
        });
        setData(arr);
      }
    };

    getRequests();
    setLoading(false);
  }, []);

  return {
    loading,
    data,
  };
};
