import React from "react";
import {
  Layout,
  Row,
  Col,
  Spin,
  Tabs,
  Card,
  Button,
  Form,
  Input,
  Select,
  Upload,
  Typography,
  Empty,
} from "antd";
import {
  CheckCircleFilled,
  UploadOutlined,
  PlusCircleFilled,
  CloseCircleFilled,
} from "@ant-design/icons";
import articleController from "./articleController";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
// old => import ArticleForm from "./articleForm";

const { Content } = Layout;
const { TabPane } = Tabs;

// articleForm
const { Option } = Select;

// article view
const { Title, Text } = Typography;

// Form
const layout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 20,
  },
};

const validateMessages = {
  // eslint-disable-next-line
  required: "${label} اجباری است!",
};

// Tags
const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

// Editor
const ReactQuillModules = {
  toolbar: [
    [{ header: "1" }, { header: "2" }, { font: [] }],
    [{ size: [] }],
    ["bold", "italic", "underline", "strike", "blockquote"],
    [
      { list: "ordered" },
      { list: "bullet" },
      { indent: "-1" },
      { indent: "+1" },
    ],
    ["link", "image", "video"],
    ["clean"],
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
};

const ReactQuillFormats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "list",
  "bullet",
  "indent",
  "link",
  "image",
  "video",
];

export default () => {
  const {
    loading,
    edit,
    articles,
    onEdit,
    // articleForm
    onFinish,
    user,
    // Upload
    fileList,
    ontransformFile,
    onRemove,
    // Editor
    editorState,
    handleEditorChange,
    // article view
    selected,
    view,
    onView,
    onBack,
    // article delete
    onDelete,
    // article confirm
    onConfirm,
  } = articleController();

  return (
    <Spin spinning={loading}>
      <Content className="inner-content rtl">
        {!edit && !view && (
          <Tabs
            defaultActiveKey="1"
            tabBarExtraContent={
              <Button icon={<PlusCircleFilled />} onClick={() => onEdit()}>
                &nbsp; &nbsp; ایجاد مقاله جدید
              </Button>
            }
          >
            <TabPane tab="تایید شده" key="1">
              <Row gutter={[24, 24]}>
                {articles &&
                  articles.map((item) => {
                    if (item.get("state") === 2) {
                      return (
                        <Col xl={6} lg={8} md={12} sm={12} xs={24}>
                          <Card
                            className="dashboard-card"
                            title="کارت مقاله تایید شده"
                            extra={
                              <Button
                                icon={<CheckCircleFilled />}
                                onClick={(e) => onView(e, item)}
                              >
                                &nbsp; &nbsp; مشاهده
                              </Button>
                            }
                          >
                            <p>عنوان : {item.get("title")}</p>
                            <p>اسلاگ : {item.get("slug")}</p>
                            <p>تگ : {item.get("tag")}</p>
                          </Card>
                        </Col>
                      );
                    }
                  })}
                {articles == null && (
                  <>
                    <Col xl={11} lg={11} md={11} sm={11} xs={11}></Col>
                    <Col xl={4} lg={4} md={4} sm={4} xs={4}>
                      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    </Col>
                  </>
                )}
              </Row>
            </TabPane>
            <TabPane tab="در انتظار تایید" key="2">
              <Row gutter={[24, 24]}>
                {articles &&
                  articles.map((item) => {
                    if (item.get("state") === 1) {
                      return (
                        <Col xl={6} lg={8} md={12} sm={12} xs={24}>
                          <Card
                            className="dashboard-card"
                            title="کارت مقاله در انتظار تایید"
                            extra={
                              <>
                                <Button
                                  style={{ marginTop: "3px" }}
                                  icon={<CloseCircleFilled />}
                                  onClick={(e) => onDelete(e, item)}
                                >
                                  &nbsp; &nbsp; حذف
                                </Button>
                                {user.get("role") == "admin" && (
                                  <>
                                    <br />
                                    <Button
                                      style={{ marginTop: "3px" }}
                                      icon={<CheckCircleFilled />}
                                      onClick={(e) => onConfirm(e, item)}
                                    >
                                      &nbsp; &nbsp; تایید
                                    </Button>
                                  </>
                                )}
                              </>
                            }
                          >
                            <p>عنوان : {item.get("title")}</p>
                            <p>اسلاگ : {item.get("slug")}</p>
                            <p>تگ : {item.get("tag").join(" / ")}</p>
                          </Card>
                        </Col>
                      );
                    }
                  })}
                {articles == null && (
                  <>
                    <Col xl={11} lg={11} md={11} sm={11} xs={11}></Col>
                    <Col xl={4} lg={4} md={4} sm={4} xs={4}>
                      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    </Col>
                  </>
                )}
              </Row>
            </TabPane>
          </Tabs>
        )}
        {/* old => <ArticleForm/> */}
        {edit && (
          <Form
            {...layout}
            name="nest-messages"
            onFinish={onFinish}
            validateMessages={validateMessages}
          >
            <Form.Item
              label="عنوان"
              name={["user", "title"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="اسلاگ"
              name={["user", "slug"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item label="متن" name={["user", "text"]}>
              <div className="editor">
                <ReactQuill
                  modules={ReactQuillModules}
                  formats={ReactQuillFormats}
                  theme="snow"
                  value={editorState}
                  onChange={handleEditorChange}
                />
              </div>
            </Form.Item>
            <Form.Item
              label="عکس"
              name={["user", "image"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Upload
                accept="image/*"
                fileList={fileList}
                transformFile={(file) => ontransformFile(file)}
                onRemove={(file) => onRemove(file)}
                disabled={false}
                maxCount={1}
              >
                <Button icon={<UploadOutlined />}>&nbsp; &nbsp;آپلود</Button>
              </Upload>
            </Form.Item>
            <Form.Item
              label="تگ ها"
              name={["user", "tag"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Select
                mode="tags"
                style={{ width: "100%" }}
                placeholder="تگ های مربوط به مقاله را انتخاب و یا اضافه کنید"
              >
                {children}
              </Select>
            </Form.Item>
            {/* <Form.Item
              label="نویسنده"
              name={["user", "author"]}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Select defaultValue="نویسنده را انتخاب کنید">
                {user &&
                  user.map((item) => {
                    return (
                      <Option value={item.get("username")}>
                        {item.get("username")}
                      </Option>
                    );
                  })}
              </Select>
            </Form.Item> */}
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
              <Button type="primary" htmlType="submit">
                ثبت مقاله
              </Button>
              &nbsp; &nbsp;
              <Button type="primary" htmlType="button" onClick={onEdit}>
                انصراف
              </Button>
            </Form.Item>
          </Form>
        )}
        {view && (
          <Row gutter={[48, 48]}>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Button onClick={() => onBack()}>بازگشت</Button>
            </Col>
            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
              <Title
                style={{
                  textAlign: "center",
                }}
                level={2}
              >
                {selected.get("title")}
              </Title>
            </Col>
            <Col xl={6} lg={4} md={3} sm={3} xs={3}></Col>
            <Col xl={12} lg={16} md={18} sm={18} xs={18}>
              <img
                alt="news"
                className="img-responsive"
                style={{ borderRadius: "30px " }}
                src={selected.get("image").url()}
              />
            </Col>
            <Col xl={6} lg={4} md={3} sm={3} xs={3}></Col>
            <Col xl={1} lg={1} md={1} sm={1} xs={1}></Col>
            <Col xl={22} lg={22} md={22} sm={22} xs={22}>
              <ReactQuill
                theme={null}
                value={selected.get("document")}
                readOnly={true}
              />
            </Col>
            <Col xl={1} lg={1} md={1} sm={1} xs={1}></Col>
          </Row>
        )}
      </Content>
    </Spin>
  );
};
