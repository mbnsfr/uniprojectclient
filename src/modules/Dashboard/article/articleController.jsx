import { useState, useEffect } from "react";
import { Parse } from "parse";

export default () => {
  const [loading, setLoading] = useState(true);
  const [edit, setEdit] = useState(false);
  const [articles, setArticles] = useState(null);
  // articleForm
  const [user, setUser] = useState(null);
  // Upload
  const [fileList, setFileList] = useState([]);
  const [file, setFile] = useState(null);
  // Editor
  const [editorState, setEditorState] = useState("");
  // article view
  const [view, setView] = useState(false);
  const [selected, setSelected] = useState(null);
  // article Edit
  const [refresh, setRefresh] = useState(false);

  const onEdit = () => {
    setFileList([]);
    setEditorState("");
    setFile(null);
    setEdit(!edit);
  };

  useEffect(() => {
    const getArticles = async () => {
      var currentUser = Parse.User.current();
      if (currentUser) {
        setUser(currentUser);
        const className = Parse.Object.extend("Articles");
        const query = new Parse.Query(className);
        if (currentUser.get("role") == "author") {
          query.equalTo("author", currentUser.get("username"));
        }
        const result = await query.find();
        if (result) {
          setArticles(result);
        }
      }
    };

    getArticles();
    setLoading(false);
  }, [edit, refresh]);

  // articleForm
  const onFinish = (values) => {
    const Articles = Parse.Object.extend("Articles");
    const articles = new Articles();

    articles.set("author", user.get("username"));
    articles.set("slug", values.user.slug);
    articles.set("tag", values.user.tag);
    articles.set("title", values.user.title);
    articles.set("image", file);
    articles.set("document", editorState);
    articles.set("state", 1);

    articles.save().then(
      (articles) => {
        setEdit(!edit);
        // alert("New object created with objectId: " + articles.id);
      },
      (error) => {
        // alert("Failed to create new object, with error code: " + error.message);
      }
    );
  };

  // Upload
  const ontransformFile = (file) => {
    if (file.type !== "") {
      setFile(
        new Parse.File(
          `articles${Math.floor(Math.random() * 100000)}`,
          file,
          file.type
        )
      );
      setFileList([
        {
          uid: "565656",
          name: "عکس کاور خبر",
          status: "done",
          url: "",
        },
      ]);
    }
  };

  const onRemove = async () => {
    setFileList([]);
  };

  // Editor
  const handleEditorChange = (editorState) => {
    setEditorState(editorState);
  };

  // article view
  const onView = (e, item) => {
    setView(!view);
    setSelected(item);
  };

  const onBack = () => {
    setView(!view);
  };

  // article delete
  const onDelete = (e, item) => {
    const className = Parse.Object.extend("Articles");
    const query = new Parse.Query(className);
    query.get(item.id).then(
      (success) => {
        success.destroy({});
        setRefresh(!refresh);
      },
      (error) => {
        setRefresh(!refresh);
        // console.log("error", error);
      }
    );
  };

  // article confirm
  const onConfirm = (e, item) => {
    const className = Parse.Object.extend("Articles");
    const query = new className();
    query.set("objectId", item.id);
    query.save().then((result) => {
      result.set("state", 2);
      return result.save();
    });
    setRefresh(!refresh);
  };

  return {
    loading,
    edit,
    articles,
    onEdit,
    // articleForm
    onFinish,
    user,
    // Upload
    fileList,
    ontransformFile,
    onRemove,
    // Editor
    editorState,
    handleEditorChange,
    // article view
    view,
    selected,
    onView,
    onBack,
    // article delete
    onDelete,
    // article confirm
    onConfirm,
  };
};
