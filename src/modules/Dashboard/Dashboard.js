import React from "react";
import "../web/master/Master.less";
import { Button, Layout, Menu, Row, Col, Typography } from "antd";
import {
  UserOutlined,
  BookOutlined,
  TeamOutlined,
  CarOutlined,
  FileUnknownOutlined,
  CloudServerOutlined,
  DownCircleOutlined,
  QuestionCircleOutlined,
  FileDoneOutlined,
} from "@ant-design/icons";
import dashboardController from "./dashboardController";
import Article from "./article/article";
import OnlineEntetainment from "./onlineEntetainment/onlineEntetainment";
import IntellectualEntertaiment from "./intellectualEntertaiment/intellectualEntertaiment";
import DownloadEntetainment from "./downloadEntetainment/downloadEntetainment";
import Request from "./request/request";
import Users from "./users/users";
import Book from "./book/book";
import profile from "../../assets/images/profile.jpg";

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;
const { Text } = Typography;

export default () => {
  const { selected, logOut, menuClick, user, type } = dashboardController();

  return (
    <Layout className="rtl">
      <Sider theme="light" trigger={null} collapsible>
        <div className="logo dashboard-profile">
          <img className="dashboard-img" src={profile} alt="profile"></img>
          <div className="dashboard-txt">{user && user.get("username")}</div>
        </div>
        <Menu
          mode="inline"
          defaultSelectedKeys={["1"]}
          onSelect={(e) => menuClick(e)}
        >
          <SubMenu
            key="sub1"
            icon={<BookOutlined />}
            title=" &nbsp; &nbsp;مقالات"
          >
            <Menu.Item key="article" icon={<FileDoneOutlined />}>
              &nbsp; &nbsp; مقاله های من
            </Menu.Item>
          </SubMenu>
          {type == "admin" && (
            <>
              <SubMenu
                key="sub2"
                icon={<CarOutlined />}
                title=" &nbsp; &nbsp;سرگرمی"
              >
                <Menu.Item
                  key="OnlineEntetainment"
                  icon={<CloudServerOutlined />}
                >
                  &nbsp; &nbsp;بازی انلاین جدید
                </Menu.Item>
                <Menu.Item
                  key="DownloadEntetainment"
                  icon={<DownCircleOutlined />}
                >
                  &nbsp; &nbsp;جدول ,کارتون و آهنگ
                </Menu.Item>
                <Menu.Item
                  key="IntellectualEntertaiment"
                  icon={<QuestionCircleOutlined />}
                >
                  &nbsp; &nbsp;چیستان و جوک
                </Menu.Item>
                <Menu.Item key="book" icon={<BookOutlined />}>
                  &nbsp; &nbsp;کتاب
                </Menu.Item>
              </SubMenu>
              <SubMenu
                key="sub3"
                icon={<TeamOutlined />}
                title=" &nbsp; &nbsp;کاربران سایت"
              >
                <Menu.Item key="users" icon={<UserOutlined />}>
                  &nbsp; &nbsp;کاربران
                </Menu.Item>
              </SubMenu>
              <SubMenu
                key="sub4"
                icon={<FileUnknownOutlined />}
                title=" &nbsp; &nbsp;درخواست ها"
              >
                <Menu.Item key="request" icon={<FileUnknownOutlined />}>
                  &nbsp; &nbsp;درخواست ها
                </Menu.Item>
              </SubMenu>
            </>
          )}
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
            backgroundColor: "#f296cd",
            overflow: "hidden",
            textAlign: "left",
          }}
        >
          {/* <Button
            style={{
              marginTop: "15px",
              marginLeft: "10px",
            }}
          >
            تغییر رمز
          </Button> */}

          <Button
            style={{
              marginTop: "15px",
              marginLeft: "10px",
            }}
            onClick={() => logOut()}
          >
            خروج
          </Button>
          <br />
          <br />
          <br />
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            minHeight: 670,
          }}
        >
          {selected === "" && (
            <Content className="inner-content rtl">
              <Row>
                <Col>
                  <Text>به داشبورد سایت سرگرمی و آموزش کودکان خوش آمدید</Text>
                </Col>
              </Row>
            </Content>
          )}
          {selected === "article" && <Article />}
          {selected === "OnlineEntetainment" && <OnlineEntetainment />}
          {selected === "IntellectualEntertaiment" && (
            <IntellectualEntertaiment />
          )}
          {selected === "DownloadEntetainment" && <DownloadEntetainment />}
          {selected === "request" && <Request />}
          {selected === "users" && <Users />}
          {selected === "book" && <Book />}
        </Content>
      </Layout>
    </Layout>
  );
};
