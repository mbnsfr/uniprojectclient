import { useState, useEffect } from "react";
import { Parse } from "parse";

export default () => {
  const [selected, setSelected] = useState("");
  const [user, setUser] = useState(null);
  const [type, setType] = useState(null);

  useEffect(() => {
    const getUser = async () => {
      var currentUser = Parse.User.current();
      if (!currentUser) {
        window.location.href = "http://localhost:3000/";
      } else {
        setUser(currentUser);
        setType(currentUser.get("role"));
      }
    };

    getUser();
  }, []);

  const logOut = async () => {
    Parse.User.logOut();
    window.location.reload();
  };

  const menuClick = (e) => {
    setSelected(e.key);
  };

  return {
    selected,
    user,
    type,
    logOut,
    menuClick,
  };
};
