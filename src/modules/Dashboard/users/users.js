import React from "react";
import { Layout, Row, Col, Spin, Table, Avatar } from "antd";
import usersController from "./usersController";
import profile from "../../../assets/images/profile.jpg";

const { Content } = Layout;

const columns = [
  {
    title: "عکس",
    key: "tags",
    dataIndex: "tags",
    render: (text, record) => <Avatar size="large" src={record.image} />,
  },
  {
    title: "نام و نام خانوادگی",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "نام کاربری",
    dataIndex: "username",
    key: "username",
  },
  {
    title: "ایمیل",
    dataIndex: "email",
    key: "email",
  },
];

export default () => {
  const { loading, user } = usersController();

  return (
    <Spin spinning={loading}>
      <Content className="inner-content rtl">
        <Row gutter={[48, 48]}>
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            <Table columns={columns} dataSource={user} />
          </Col>
        </Row>
      </Content>
    </Spin>
  );
};
