import { useState, useEffect } from "react";
import { Parse } from "parse";
import profile from "../../../assets/images/profile.jpg";

export default () => {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState(null);

  useEffect(() => {
    const getUser = async () => {
      const className = Parse.Object.extend("User");
      const query = new Parse.Query(className);
      query.equalTo("role", "author");
      const result = await query.find({useMasterKey: true});
      if (result) {
        let arr = [];
        result.map((item, key) => {
          arr.push({
            key: key,
            name: item.get("firstName") + " " + item.get("lastName"),
            username: item.get("username"),
            email: item.get("email"),
            image: item.get("image") ? item.get("image").url() : profile,
          });
        });
        setUser(arr);
      }
    };

    getUser();
    setLoading(false);
  }, []);

  // article delete
  // const onDelete = (e, item) => {
  //   const className = Parse.Object.extend("Articles");
  //   const query = new Parse.Query(className);
  //   query.get(item.id).then(
  //     (success) => {
  //       success.destroy({});
  //       setRefresh(!refresh);
  //     },
  //     (error) => {
  //       // console.log("error", error);
  //     }
  //   );
  // };

  return {
    loading,
    user,
  };
};
