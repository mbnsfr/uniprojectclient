import { useState, useEffect } from "react";
import { Parse } from "parse";

export default () => {
  const [loading, setLoading] = useState(true);
  const [edit, setEdit] = useState(false);
  const [entertainment, setEntertainment] = useState(null);
  // Upload
  const [fileList, setFileList] = useState([]);
  const [file, setFile] = useState(null);
  // article view
  const [view, setView] = useState(false);
  const [selected, setSelected] = useState(null);
  // refresh
  const [refresh, setRefresh] = useState(false);
  // Editor
  const [editorState, setEditorState] = useState("");

  const onEdit = () => {
    setEditorState("");
    setFileList([]);
    setFile(null);
    setEdit(!edit);
  };

  useEffect(() => {
    const getEntertainment = async () => {
      const className = Parse.Object.extend("Entertainment");
      const query = new Parse.Query(className);
      query.equalTo("type", "intellectual");
      const result = await query.find();
      if (result) {
        setEntertainment(result);
      }
    };

    getEntertainment();
    setLoading(false);
  }, [edit, refresh]);

  // articleForm
  const onFinish = (values) => {
    const Entertainment = Parse.Object.extend("Entertainment");
    const entertainment = new Entertainment();

    entertainment.set("name", values.user.name);
    entertainment.set("text", values.user.text);
    entertainment.set("image", file);
    entertainment.set("state", 1);
    entertainment.set("document", editorState);
    entertainment.set("type", "download");

    entertainment.save().then(
      (entertainment) => {
        setEdit(!edit);
        // alert("New object created with objectId: " + entertainment.id);
      },
      (error) => {
        setEdit(!edit);
        // alert("Failed to create new object, with error code: " + error.message);
      }
    );
  };

  // Editor
  const handleEditorChange = (editorState) => {
    setEditorState(editorState);
  };

  // Upload
  const ontransformFile = (file) => {
    if (file.type !== "") {
      setFile(
        new Parse.File(
          `entertainment${Math.floor(Math.random() * 100000)}`,
          file,
          file.type
        )
      );
      setFileList([
        {
          uid: "565656",
          name: "عکس کاور سرگرمی",
          status: "done",
          url: "",
        },
      ]);
    }
  };

  const onRemove = async () => {
    setFileList([]);
  };

  // article view
  const onView = (e, item) => {
    setView(!view);
    setSelected(item);
  };

  const onBack = () => {
    setView(!view);
  };

  // article delete
  const onDelete = (e, item) => {
    const className = Parse.Object.extend("Entertainment");
    const query = new Parse.Query(className);
    query.get(item.id).then(
      (success) => {
        success.destroy({});
        setRefresh(!refresh);
      },
      (error) => {
        setRefresh(!refresh);
        // console.log("error", error);
      }
    );
  };

  // article confirm
  const onConfirm = (e, item) => {
    const Entertainment = Parse.Object.extend("Entertainment");
    const entertainment = new Entertainment();
    entertainment.set("objectId", item.id);
    entertainment.save().then((result) => {
      result.set("state", 2);
      return result.save();
    });
    setRefresh(!refresh);
  };

  return {
    loading,
    edit,
    entertainment,
    onEdit,
    // articleForm
    onFinish,
    // Upload
    fileList,
    ontransformFile,
    onRemove,
    // article view
    view,
    selected,
    onView,
    onBack,
    // article delete
    onDelete,
    // article confirm
    onConfirm,
    // Editor
    editorState,
    handleEditorChange,
  };
};
