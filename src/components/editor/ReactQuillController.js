import { useState, useEffect } from "react";
import { Parse } from "parse";

export default () => {
  const [editorState, setEditorState] = useState(null);
  const [objId, setObjId] = useState(true);

  useEffect(() => {
    const getEditorState = async () => {
      const className = Parse.Object.extend("Articles");
      const query = new Parse.Query(className);
      const result = await query.first();
      if (result) {
        setObjId(result.id);
        setEditorState(result.get("document"));
      }
    };
    getEditorState();
  }, []);

  const submitContent = async () => {
    const className = Parse.Object.extend("Articles");
    const query = new Parse.Query(className);
    query.equalTo("objectId", objId);
    const result = await query.first();
    if (result) {
      result.set("document", editorState);
      result.save().then(
        (success) => {
          alert("فایل ذخیره شد");
        },
        (error) => {
          alert(error.responseText);
        }
      );
    }
  };

  const handleEditorChange = (editorState) => {
    setEditorState(editorState);
  };

  return {
    // values
    editorState,

    // functions
    submitContent,
    handleEditorChange,
  };
};
