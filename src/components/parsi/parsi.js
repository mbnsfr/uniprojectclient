import dayjs from 'dayjs';

export function toPersianNumber(n) {
  const farsiDigits = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

  return n.toString().replace(/\d/g, (x) => farsiDigits[x]);
}

export function toPersianDate(d, format = "DD MMMM YYYY") {
  return toPersianNumber(
    dayjs(d)
      .calendar("jalali")
      .locale("fa")
      .format(format)
  );
}
