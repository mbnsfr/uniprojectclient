import React from "react";
import Quiz from "react-quiz-component";

// quiz obj => {
//   quizTitle "string"
//   quizSynopsis "string"
//   appLocale obj of array => [
//    "landingHeaderText": "<questionLength> Questions",
//    "question": "Question",
//    "startQuizBtn": "Start Quiz",
//    "resultFilterAll": "All",
//    "resultFilterCorrect": "Correct",
//    "resultFilterIncorrect": "Incorrect",
//    "nextQuestionBtn": "Next",
//    "resultPageHeaderText": "You have completed the quiz. You got <correctIndexLength> out of <questionLength> questions.",
//    "resultPagePoint": "You scored <correctPoints> out of <totalPoints>.",
//    "singleSelectionTagText": "Single Selection",
//    "multipleSelectionTagText": "Multiple Selection",
//    "pickNumberOfSelection": "Pick <numberOfSelection>"
//   ]
//   questions array of obi's => [
//    {
//      question "string"
//      questionType "text/photo"
//      questionPic *if you need to display Picture in Question*
//      answerSelectionType "single/multiple"
//      answers array of strings => ["string/link to image","string/link to image"]
//      correctAnswer "string of the number of the answers[]"
//      messageForCorrectAnswer "string"
//      messageForIncorrectAnswer "string"
//      explanation "string"
//      point "string"
//    }
//   ]
// }
// questionPic: "https://dummyimage.com/600x400/000/fff&text=X", // if you need to display Picture in Question
//   answers: [
//     "https://dummyimage.com/600x400/000/fff&text=A",
//     "https://dummyimage.com/600x400/000/fff&text=B",
//     "https://dummyimage.com/600x400/000/fff&text=C",
//     "https://dummyimage.com/600x400/000/fff&text=D",
//   ],

export default () => {
  const quiz = {
    quizTitle: "یه کويیز کوچولو",
    quizSynopsis:
      "در این تست یتونید میزان اطلاعات عمومی خودتونو بسنجید و با مقاله های بیشتری اشنا شید . اگر دوست دارید کويیز رو شروع کنید",
    questions: [
      {
        question: "سوال ۱",
        questionType: "text",
        answerSelectionType: "single",
        answers: ["۱", "۲", "۳", "۴"],
        correctAnswer: "3",
        messageForCorrectAnswer: "افرین درست بود",
        messageForIncorrectAnswer: "دفه بعد درست جواب بده 3<",
        explanation: "متبیسثطزرذادتنکمنتقظیبزلذدمگکمنغتیطز.",
        point: "20",
      },
      {
        question: "سوال ۲",
        questionType: "text",
        answerSelectionType: "single",
        answers: ["۱", "۲", "۳", "۴"],
        correctAnswer: "2",
        messageForCorrectAnswer: "افرین درست بود",
        messageForIncorrectAnswer: "دفه بعد درست جواب بده 3<",
        explanation: "متبیسثطزرذادتنکمنتقظیبزلذدمگکمنغتیطز.",
        point: "20",
      },
      {
        question: "سوال ۳",
        questionType: "text",
        answerSelectionType: "single",
        answers: ["درست", "غلط"],
        correctAnswer: "2",
        messageForCorrectAnswer: "افرین درست بود",
        messageForIncorrectAnswer: "دفه بعد درست جواب بده 3<",
        explanation: "متبیسثطزرذادتنکمنتقظیبزلذدمگکمنغتیطز.",
        point: "10",
      },
      {
        question: "سوال ۴",
        questionType: "text",
        answerSelectionType: "single",
        answers: ["۱", "۲", "۳", "۴"],
        correctAnswer: "3",
        messageForCorrectAnswer: "افرین درست بود",
        messageForIncorrectAnswer: "دفه بعد درست جواب بده 3<",
        explanation: "متبیسثطزرذادتنکمنتقظیبزلذدمگکمنغتیطز.",
        point: "30",
      },
      {
        question: "سوال ۳",
        questionType: "text",
        answerSelectionType: "multiple",
        answers: ["۱", "۲", "۳", "۴"],
        correctAnswer: [1, 2, 4],
        messageForCorrectAnswer: "افرین درست بود",
        messageForIncorrectAnswer: "دفه بعد درست جواب بده 3<",
        explanation: "متبیسثطزرذادتنکمنتقظیبزلذدمگکمنغتیطز.",
        point: "20",
      },
    ],
    appLocale: {
      landingHeaderText: "<questionLength> سوال",
      question: "سوال",
      startQuizBtn: " شروع",
      resultFilterAll: "همه",
      resultFilterCorrect: "سوالای درست",
      resultFilterIncorrect: "سوالای اشتباه",
      nextQuestionBtn: "بعدی",
      resultPagePoint: "شما  <correctIndexLength>  سوال رو درست جواب دادید",//<totalPoints>
      resultPageHeaderText:
        "شما کويیز رو تموم کردید . جوابای درست : <correctIndexLength> ", //out of <questionLength> questions.
      singleSelectionTagText: "سوال یک جواب دارد",
      multipleSelectionTagText: "سوال چند جواب دارد",
      pickNumberOfSelection: "<numberOfSelection>  عدد را انتخاب کنید   ",
    },
  };

  return <Quiz quiz={quiz} shuffle={true} />;
};
