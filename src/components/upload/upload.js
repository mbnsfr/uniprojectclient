import React from "react";
import { Upload, Button, Row, Col, Spin } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import uploadController from "./uploadController";

export default () => {
  const {
    fileList,
    loading,

    ontransformFile,
    onRemove,
  } = uploadController();

  return (
    <Spin spinning={loading} size="large">
      <Row gutter={[48, 48]}>
        <Col span={12}>
          <br />
          <Upload
            accept="image/*"
            fileList={fileList}
            transformFile={(file) => ontransformFile(file)}
            onRemove={(file) => onRemove(file)}
            disabled={false}
          >
            <Button icon={<UploadOutlined />}>&nbsp; &nbsp;آپلود</Button>
          </Upload>
        </Col>
      </Row>
    </Spin>
  );
};
