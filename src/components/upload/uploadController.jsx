import { useState, useEffect } from "react";
import { Parse } from "parse";
import { toPersianNumber } from "../parsi/parsi";

export default () => {
  const [fileList, setFileList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [force, setForce] = useState(true);

  useEffect(() => {
    const getFileList = async () => {
      const className = Parse.Object.extend("Articles");
      const query = new Parse.Query(className);
      const result = await query.find();
      if (result) {
        let arr = [];
        // eslint-disable-next-line
        result.map((item, key) => {
          if (item.get("image")) {
            arr.push({
              uid: `${item.id}`,
              name: `عکس اپلود شده -${toPersianNumber(key + 1)}`,
              status: "done",
              url: `${item.get("image")._url}`,
            });
          }
        });
        setFileList(arr);
      }
    };
    getFileList();
    setLoading(false);
  }, [force]);

  const ontransformFile = (file) => {
    if (file.type !== "") {
      var parseFile = new Parse.File(
        `articles${Math.floor(Math.random() * 100000)}`,
        file,
        file.type
      );
      var className = new Parse.Object("Articles");
      className.set("state", 1);
      className.set("image", parseFile);
      className.save().then(
        (success) => {
          if (success.get("image")) {
            let arr = fileList;
            arr.push({
              uid: `${success.id}`,
              name: `عکس اپلود شده -${arr.length + 1}`,
              status: "done",
              url: `${success.get("image")._url}`,
            });
            setFileList(arr);
          }
        },
        (error) => {
          alert(error.responseText);
        }
      );
      setForce(!force);
    } else {
      alert("فرمت فایل شما مشکل دارد فایلی با فرمت دیگر انتخاب کنید");
    }
  };

  const onPreview = (file) => {
    // console.log("onPreview", file);
  };

  const onRemove = async (file) => {
    const className = Parse.Object.extend("Articles");
    const query = new Parse.Query(className);
    query.equalTo("objectId", file.uid);
    const result = await query.first();
    if (result) {
      result.unset("image");
      result.save().then(
        (success) => {
          let arr = fileList;
          // eslint-disable-next-line
          arr.map((item) => {
            if (item.uid === file.uid) {
              arr.pop(item);
            }
          });
          setFileList(arr);
          setForce(!force);
        },
        (error) => {
          alert("فایل مورد نظر پاک نشد دوباره تلاش کنید");
        }
      );
    }
  };

  return {
    // values
    fileList,
    loading,

    // setValues
    setFileList,
    setLoading,

    // functions
    ontransformFile,
    onPreview,
    onRemove,
  };
};
